const http = require('http');
const app = require('./app.js');


//Definir el puerto de las variables de entorno, si no hay usar puerto 3000
const port = process.env.PORT || 3000;

const server = http.createServer(app);
server.listen(port)
