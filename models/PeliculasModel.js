const mysqlConnection = require('../db/db')

const getPeliculas = () => {
    return new Promise((resolve, reject) =>  {
        mysqlConnection.query('SELECT * from peliculas where estado = 1', (err, rows)=>{
            if(err){
                return reject(err);
            }else{
                resolve(rows);
            }
        })
    })
}

const getPeliculasFilter = (clasificacion, idioma, fechaInicio, fechaFin) => {
    return new Promise((resolve, reject) =>  {
        mysqlConnection.query('SELECT * from peliculas where clasificacion = ? and idioma =? and fecha >= ? and fecha <= ? and estado = 1', 
        [clasificacion, idioma, fechaInicio, fechaFin], (err, rows)=>{
            if(err){
                return reject(err);
            }else{
                resolve(rows);
            }
        })
    })
}


const getPeliculasAdmin = () => {
    return new Promise((resolve, reject) =>  {
        mysqlConnection.query('SELECT * from peliculas', (err, rows)=>{
            if(err){
                return reject(err);
            }else{
                resolve(rows);
            }
        })
    })
}

const insertPeliculas = ({nombre, idioma, clasificacion, duracion, fecha, url, sinopsis, director, reparto, poster, estado}) => {
    return new Promise((resolve, reject) => {
        mysqlConnection.query(
            'INSERT INTO peliculas (nombre, idioma, clasificacion, duracion, fecha, url, sinopsis, director, reparto, poster, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
        [nombre, idioma, clasificacion, duracion, fecha, url, sinopsis, director, reparto, poster, estado], (err, rows) =>{
            if(err){
                return reject(err);
            }else{
                resolve(rows);
            }
        })
    })
}

const getPeliculasById = (idMovie) =>{
    return new Promise((resolve, reject)=>{
            mysqlConnection.query('SELECT * from peliculas where idMovie = ?', [idMovie], (err, rows)=>{
                if(err){
                    return reject(err)
                }else{
                    resolve(rows[0])
                }
            })
    })
}

const cambiarEstadoPeli = (estado, idMovie) =>{
  
    return new Promise((resolve, reject)=>{
        mysqlConnection.query('UPDATE peliculas SET estado = ? WHERE idMovie = ?', [estado, idMovie], (err, rows)=>{
            if(err){
                return reject(err)
            }else{
                resolve(rows)
            }
        })
    })
}


const editarPelicula = (nombre, idioma, clasificacion, duracion, fecha, url, sinopsis, director, reparto, poster, idMovie) =>{
  
    return new Promise((resolve, reject)=>{
        mysqlConnection.query(
            'UPDATE peliculas SET nombre = ?, idioma = ?, clasificacion = ?, duracion = ?, fecha = ?, url = ?, sinopsis = ?, director = ?, reparto = ?, poster = ? WHERE idMovie = ?', 
        [nombre, idioma, clasificacion, duracion, fecha, url, sinopsis, director, reparto, poster, idMovie], (err, rows)=>{
            if(err){
                return reject(err)
            }else{
                resolve(rows)
            }
        })
    })
}


const deletePeli = (idMovie) =>{
  
    return new Promise((resolve, reject)=>{
        mysqlConnection.query('DELETE from peliculas WHERE idMovie = ?', [idMovie], (err, rows)=>{
            if(err){
                return reject(err)
            }else{
                resolve(rows)
            }
        })
    })
}

module.exports = {
    getPeliculas : getPeliculas,
    insertPeliculas : insertPeliculas,
    getPeliculasById : getPeliculasById,
    cambiarEstadoPeli : cambiarEstadoPeli,
    getPeliculasAdmin : getPeliculasAdmin,
    deletePeli : deletePeli,
    getPeliculasFilter: getPeliculasFilter,
    editarPelicula : editarPelicula
}