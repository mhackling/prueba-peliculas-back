const mysqlConnection = require('../db/db');

const getUsuarios = () =>{
    return new Promise((resolve, reject) => {
        mysqlConnection.query('SELECT * from usuarios', (err, rows) =>{
            if(err){
                return reject(err);
            } 
            resolve(rows);
        })
    })
}

const getUsuarioByCorreo = (correo) =>{
    return new Promise((resolve, reject) => {
        mysqlConnection.query('SELECT * from usuarios where correo = ?', [correo], (err, rows) =>{
            if(err){
                return reject(err);
            }else{
                resolve(rows[0]);
            }
        })
    });
}

const insertUsuario = ({nombre, correo, password, usuarioscol}) => {
    return new Promise((resolve, reject) => {
        mysqlConnection.query('INSERT INTO usuarios (nombre, correo, password, usuarioscol) VALUES (?, ?, ?, ?)', 
        [nombre, correo, password, usuarioscol], (err, rows) =>{
            if(err){
                return reject(err);
            }else{
                resolve(rows);
            }
        })
    })
}

module.exports = {
    getUsuarios : getUsuarios,
    getUsuarioByCorreo : getUsuarioByCorreo,
    insertUsuario : insertUsuario
}