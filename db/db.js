const mysql = require('mysql2');

const mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Admin123',
    port: '3306',
    database: 'prueba_sql',
})

mysqlConnection.connect( err => {
    if(err){
        console.log("Error en db: ",err)
        return;
    }else{
        console.log("Conectado a db")
    }
})

module.exports = mysqlConnection;