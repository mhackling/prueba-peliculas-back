//Inicializar express
const express = require('express');
const app = express(); 
const bodyParser = require('body-parser');
const cors = require('cors');


//Requerimos que la app use json
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.use(cors()); 

//

//Rutas
const userRoute = require('./routes/usuarios');
const peliculasRoute = require('./routes/peliculas')


//Usar rutas
app.use('/user', userRoute)
app.use('/peliculas', peliculasRoute)
app.use(express.static('files')); 


module.exports = app;