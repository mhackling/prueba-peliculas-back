APLICATIVO DEL LADO DEL SERVIDOR BASADO EN NODE.JS CON EXPRESS PARA LA CREACION, EDICION Y ELIMINACION DE PELICULAS EN CARTELERA

#Instalación 
Para correr el aplicativo solo tenemos que ejecutar el comando npm install que descargará todas las dependencias del node package manager (npm) necesarias para el funcionamiento del aplicativo.

#Scripts

Hay dos scripts para correr el aplicativo, npm run dev (en modo de prueba y desarrollo con nodemon) y npm run prod que no es modo de dev


#Base de datos 
Dentro de la ruta del backend db/db.js => constante mysqlConnection estarán las credenciales para ingresar a la base de datos (mysql) de manera local (así que depende de la instancia local que tengas el como configuraremos la conección)

Además, he incluido dos scripts dentro de la carpeta del proyecto (prueba_sql_peliculas y prueba_sql_usuarios) donde están los comandos de creación de tablas y columnas que uso