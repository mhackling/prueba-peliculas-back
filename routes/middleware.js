const jwt = require('jsonwebtoken');
const moment = require('moment');
const key = require('../settings/key')

const checkToken = (req, res, next) => {
    if(!req.headers['user_token'])
        return res.json({
            error : "No se ha incluido un header"
        });
        const token = req.headers['user_token'];
        let payload = null;
        try {
            payload = jwt.decode(token, key)
        } catch (err) {
            return res.json({
                message : 'Token invalido'
            });
        }
        if (moment().unix() > payload.expiresAt) {
            return res.json({message : 'Token expirado'});
        };
        req.id = payload.id;
        next();
}


module.exports = {
    checkToken : checkToken
}
