const express = require('express');
const router = express.Router();
const usuarioModel = require('../models/UsuarioModel');
const jwt = require('jsonwebtoken')
const moment = require('moment');
const key = require('../settings/key')
const bcrypt = require('bcrypt');
const middleware = require('./middleware')

router.get('/listUser', async (req, res) => {
    usuarioModel.getUsuarios().then(rows=>{
        res.json(rows);
    }).catch(err=>{
        console.log(err);
    })
});

router.post('/register', async (req, res) => {
    console.log(req.body);
    req.body.password = bcrypt.hashSync(req.body.password, 10);
    const result = await usuarioModel.insertUsuario(req.body);
    res.json({
        mensaje: "Usuario creado!",
        datos: result,
    })
});

const createToken = (user) => {
    let payload = {
        id: user.id,
        nameuser: user.nombre,
        createdAt: moment().unix(),
        expiresAt: moment().add(1, 'hour').unix()
    }
    return jwt.sign(payload, key);
}


router.post('/login', async (req, res) => {
    const user = await usuarioModel.getUsuarioByCorreo(req.body.correo)
    if(user === undefined){
        res.json({
            error: "Credenciales incorrectas",
            status: false
        })
    }else{
        const equals = bcrypt.compareSync(req.body.password, user.password);
        if(equals){
            
            res.json({
                token: createToken(user),
                nameuser: user.nombre,
                mensaje: "Inicio exitoso",
                status: true
            })
        } else{
            res.json({
                error: "Login erroneo",
                status: false
            })
        }
    }
})

router.use(middleware.checkToken);

module.exports = router;