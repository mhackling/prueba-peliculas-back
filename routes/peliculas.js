const express = require('express');
const router = express.Router();
const peliculasModel = require('../models/PeliculasModel')
const upload = require('../settings/storage')

router.get('/listPeliculas', async (req, res) => {
    peliculasModel.getPeliculas().then(rows=>{
        res.json(rows);
    }).catch(err=>{
        console.log(err);
    })
})

router.get('/listPeliculasAdmin', async (req, res) => {
    peliculasModel.getPeliculasAdmin().then(rows=>{
        res.json(rows);
    }).catch(err=>{
        console.log(err);
    })
})

router.post('/subirArchivo', upload.single('image'), async (req, res)=>{
    res.json({
        mensaje: "archivo subido"
    })
})


router.post('/nuevaPelicula', async (req, res)=>{
    console.log(req.body)
    peliculasModel.insertPeliculas(req.body).then(rows => {
        res.json(rows);
    }).catch(err=>{
        console.log(err);
    })
})

router.delete('/deletePelicula/:idMovie', async (req, res)=>{
    peliculasModel.deletePeli(req.params.idMovie).then(rows => {
        res.json({
            mensaje: 'Pelicula eliminada!',
            result: rows
        })
    }).catch(err=>{
        console.log(err)
    })
})

router.get('/getPeliculasFilter/:clasificacion/:idioma/:fechaInicio/:fechaFin', async(req, res)=>{
        const {clasificacion, idioma, fechaInicio, fechaFin} = req.params
        peliculasModel.getPeliculasFilter(clasificacion, idioma, fechaInicio, fechaFin).then(rows =>{
            res.json({
                mensaje: 'Filtros aplicados!',
                result: rows
            })
        }).catch(err=>{
            console.log(err)
        })
})

router.put('/editarPelicula', async(req, res)=>{
    const { nombre, idioma, clasificacion, duracion, fecha, url, sinopsis, director, reparto, poster, idMovie } = req.body
    peliculasModel.editarPelicula(nombre, idioma, clasificacion, duracion, fecha, url, sinopsis, director, reparto, poster, idMovie).then(rows=>{
        res.json({
            mensaje: 'Pelicula editada!',
            result: rows
        })
    }).catch(err=>{
        console.log(err)
    })
})

router.put('/cambiarEstado', async(req, res)=>{
    if(req.body.estado){
        req.body.estado = false;
    }else{
        req.body.estado = true;
    }

    const {estado, idMovie } = req.body
    
    peliculasModel.cambiarEstadoPeli(estado, idMovie).then(rows=>{
        res.json({
            mensaje: 'Estado cambiado!',
            result: rows
        });
    }).catch(err=>{
        console.log(err);
    })   
})

module.exports = router;